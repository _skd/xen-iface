XEN-IFACE(8) - System Manager's Manual

# NAME

**xen-iface** - inform Xen of IPs bound to the VM

# SYNOPSIS

**xen-iface**
\[**-cd**]

# DESCRIPTION

**xen-iface**
is a utility that informs Xen of the IP addresses bound to the VM virtual
interfaces.
Optionally, all interfaces within the XenStore with IP addresses assigned can
be cleared before being updated.

The following command first needs to be run on the Xen host.

	# xe vm-param-set uuid=<VM_UUID> platform:viridian=false

This script can be called from
*/etc/rc.local*
to allow interface information to be exchanged on boot.

The options are as follows:

**-c**

> Clear any IP addresses bound to XenStore vif keys.

**-d**

> Debug mode.
> Print the
> hostctl(8)
> commands and exit.

# EXIT STATUS

The **xen-iface** utility exits&#160;0 on success, and&#160;&gt;0 if an error occurs.

# SEE ALSO

pvbus(4),
xen(4),
hostctl(8),
rc(8)

# AUTHORS

Sean Davies &lt;[skd@skd.id.au](mailto:skd@skd.id.au)&gt;

OpenBSD 7.2 - February 25, 2023
