#!/bin/sh
#
# Copyright (c) 2023 Sean Davies <skd@skd.id.au>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE

die()
{
	echo "$1" 1>&2
	exit 1
}

usage()
{
	die "usage: ${0##*/} [-cd]"
}

cflag=0
dflag=0
while getopts cd arg; do
	case ${arg} in
	c)	cflag=1 ;;
	d)	dflag=1 ;;
	*)	usage ;;
	esac
done
shift $((OPTIND - 1))
[ "$#" -eq 0 ] || usage

if [ "${dflag}" -eq 0 ] && [ "$(id -u)" -ne 0 ]; then
	die "${0##*/}: needs root privileges"
fi

hostctl device >/dev/null 2>&1 || die "${0##*/}: unsupported device"
if [ "${cflag}" -eq 1 ]; then
	for vifidx in $(hostctl device/vif); do
		for af in ipv4 ipv6; do
			for ipidx in $(hostctl "attr/vif/${vifidx}/${af}"); do
				addr=$(hostctl "attr/vif/${vifidx}/${af}/${ipidx}")
				[ -n "${addr}" ] || continue

				case ${dflag} in
				0)	hostctl "attr/vif/${vifidx}/${af}/${ipidx}" '' ;;
				1)	echo "hostctl attr/vif/${vifidx}/${af}/${ipidx} ''" ;;
				esac
			done
		done
	done
fi

ifs=$(dmesg | grep "at.*address" | sort -u | cut -d ' ' -f 1)
for if in ${ifs}; do
	for af in ipv4 ipv6; do
		case ${af} in
		ipv4)	addrs=$(ifconfig "${if}" | grep -w inet | cut -d ' ' -f 2) ;;
		ipv6)	addrs=$(ifconfig "${if}" | grep -w inet6 | cut -d ' ' -f 2 | cut -d % -f 1) ;;
		esac
		[ -n "${addrs}" ] || continue

		ifidx=$(echo "${if}" | tr -d '[:alpha:]')
		ipidx=0
		for addr in ${addrs}; do
			case ${dflag} in
			0)	hostctl "attr/vif/${ifidx}/${af}/${ipidx}" "${addr}" ;;
			1)	echo "hostctl attr/vif/${ifidx}/${af}/${ipidx} ${addr}" ;;
			esac
			ipidx=$((ipidx + 1))
		done
	done
done
